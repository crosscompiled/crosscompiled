# What is this?

Crosscompiled is a collection of cross compilers which makes it easy to cross compile C/C++code to various platforms and architectures.

# The Need

I found very challenging to cross compile C/C++code especially if you have multiple target platforms and architectures.

The idea is to have a one liner to cross compile for any target

# Implementation

## Utilized technologies

* [Docker](https://docker.com/)
* [Bootlin's toolchains](https://toolchains.bootlin.com/)
* [Gitlab](https://gitlab.com/)
* [Quay](https://quay.io/)

Every toolchain is packaged into a container image and ready to be used to cross compile source code.

# Example

```
cd source-code
docker run -it -u $(id -u ${USER}):$(id -g ${USER}) --rm -v $(pwd):/w -w /w --entrypoint make quay.io/crosscompiled/tool:bootlin--microblazebe--uclibc--stable-2018.11-1
```

This will call **make** on your app and should create a _microblazebe_ architecture based binary for a _uclibc_ linux
